#ifndef _T_STDLIB_
	#define _T_STDLIB_
	
	#ifdef UNICODE
		#define tcerr wcerr
		#define tcout wcout
		#define tstring wstring
	#else
		#define tcerr cerr
		#define tcout cout
		#define tstring string
	#endif
#endif
