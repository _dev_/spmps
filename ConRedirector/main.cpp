#include "main.h"

extern int startRouting(_TCHAR *listenIP, int listenPort, _TCHAR *targetIP, int targetPort);

int _tmain(int argc, _TCHAR *argv[]) {
	_TCHAR *listenIP = NULL, *targetIP = NULL;
	int listenPort = -1, targetPort = -1;
	{
		for (int i = 1; i < argc; ++i) {
			_TCHAR* param = argv[i];
			for (unsigned int j = 0; j < _tcslen(param); j++)
				param[j] = _totlower(param[j]);
			//std::tcout << i << _T(": ") << param << std::endl;

			if (_tcscmp(param, _T("-li")) == 0) {
				listenIP = argv[++i];
				//std::tcout << _T("listenIP = ") << listenIP << std::endl;
			} else if (_tcscmp(param, _T("-lp")) == 0) {
				listenPort = _ttoi(argv[++i]);
				//std::tcout << _T("listenPort = ") << listenPort << std::endl;
			} else if (_tcscmp(param, _T("-ti")) == 0) {
				targetIP = argv[++i];
				//std::tcout << _T("targetIP = ") << targetIP << std::endl;
			} else if (_tcscmp(param, _T("-tp")) == 0) {
				targetPort = _ttoi(argv[++i]);
				//std::tcout << _T("targetPort = ") << targetPort << std::endl;
			} else
				std::tcerr << _T("Invalid parameter: ") << param << std::endl;
		}
		//std::tcerr << (listenIP == NULL) << ' ' << (targetIP == NULL) << ' ' << listenPort << ' ' << targetPort << std::endl;
	}

	if (listenIP == NULL || targetIP == NULL || listenPort == -1 || targetPort == -1) {
		//std::tcerr << (listenIP == NULL) << ' ' << (targetIP == NULL) << ' ' << listenPort << ' ' << targetPort << std::endl;
		std::tcerr << _T("Usage: ") << argv[0] <<
			_T(" -li <listening IP> -lp <listening port> -ti <target IP> -tp <target port>") << std::endl;
		return 1;
	}

	WSAData wsa;
	int err;
	if ((err = WSAStartup(MAKEWORD(1, 1), &wsa)) != 0) {
		std::tcerr << _T("Cannot initialize WinSock 1.1: error code ") << err << std::endl;
		return err;
	}

	int re = startRouting(listenIP, listenPort, targetIP, targetPort);

	WSACleanup();
	return re;
}
