#include <iostream>
#include <list>

#include <tchar.h>
#include <WinSock.h>

#include "tstd.h"

#define NET_IO_BUFFER_SIZE (1 << 16)

#define _DEBUG_SOCKET_LIFECYCLE_
#define _DEBUG_DATA_TRANSFER_

typedef struct {
	SOCKET clientSocket;
	char clientBuf[NET_IO_BUFFER_SIZE];
	int clientBufLen;

	SOCKET targetSocket;
	char targetBuf[NET_IO_BUFFER_SIZE];
	int targetBufLen;
	bool targetReady;
} RedirectedConnection;
std::list<RedirectedConnection*> connections;

unsigned long resolveAddress(const _TCHAR* hostname);
unsigned long _tinet_addr(const _TCHAR*);

int startRouting(_TCHAR *listenIP, int listenPort, _TCHAR *targetIP, int targetPort);
SOCKET startServer(_TCHAR *ip, int port);
void acceptAndConnect(SOCKET &serverSocket, sockaddr *targetAddr);
SOCKET startConnection(RedirectedConnection *con, sockaddr *targetAddr);
int setupSocketSets(fd_set *read, fd_set *write, fd_set *exc, SOCKET &serverSocket);
bool disconnect(SOCKET clientSocket);
bool readFromClient(RedirectedConnection *con);
bool sendToClient(RedirectedConnection *con);
bool readFromTarget(RedirectedConnection *con);
bool sendToTarget(RedirectedConnection *con);
