#include "redirector.h"

char* _tNewValueOf(const _TCHAR *tc) {
	char *val;
	size_t size;
	#ifdef UNICODE
		size = WideCharToMultiByte(CP_UTF8, 0, tc, _tcslen(tc), NULL, 0, NULL, NULL);
		val = new char[size + 1];
		WideCharToMultiByte(CP_UTF8, 0, tc, _tcslen(tc), val, size, NULL, NULL);
		val[size] = 0;
	#else
		size = _tcslen(tc);
		val = new char[size + 1];
		memcpy(val, tc, size);
		val[size] = 0;
	#endif
	return val;
}

unsigned long _tinet_addr(const _TCHAR *ip)
{
	char* addr = _tNewValueOf(ip);
	unsigned long re = inet_addr(addr);
	delete addr;
	return re;
}

int startRouting(_TCHAR *listenIP, int listenPort, _TCHAR *targetIP, int targetPort) {
	SOCKET ss = startServer(listenIP, listenPort);
	if (ss == INVALID_SOCKET) {
		int err = WSAGetLastError();
		std::tcerr << _T("Cannot start listener: error code ") << err << std::endl;
		return err;
	}
	
	sockaddr_in target;
	{
		unsigned long targetAddr = resolveAddress(targetIP);
		if (targetAddr == INADDR_NONE || targetAddr == INADDR_ANY) {
			std::tcerr << _T("Invalid target address: ") << targetIP << std::endl;
			return 1;
		}

		target.sin_family = AF_INET;
		target.sin_port = htons(targetPort);
		target.sin_addr.s_addr = targetAddr;
	}

	std::tcout << _T("Listening for connections on ") << listenIP << ':' << listenPort << std::endl;
	std::tcout << _T("Forwarding connections to ") << targetIP << ':' << targetPort << std::endl;
	while (true) {
		acceptAndConnect(ss, (sockaddr*) &target);
		std::tcout << _T("Continuing listening...") << std::endl;
	}
}

SOCKET startServer(_TCHAR *ip, int port) {
	unsigned long listenAddr = /*_tinet_addr*/resolveAddress(ip);
	if (listenAddr == INADDR_NONE)
		return INVALID_SOCKET;

	SOCKET re = socket(AF_INET, SOCK_STREAM, 0);
	if (re != INVALID_SOCKET) {
		sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr.s_addr = listenAddr;

		if (bind(re, (sockaddr*) &addr, sizeof(addr)) != 0) {
			std::tcerr << _T("Cannot bind on ") << ip << ':' << port << _T(", error code ") << WSAGetLastError() << std::endl;
			return INVALID_SOCKET;
		}

		listen(re, SOMAXCONN);
	}
	return re;
}

void acceptAndConnect(SOCKET &serverSocket, sockaddr *targetAddr) {
	while (true) {
		fd_set read, write, exc;
		int nfds = setupSocketSets(&read, &write, &exc, serverSocket);

		// block until event
		if (select(nfds, &read, &write, &exc, NULL) > 0) {
			if (FD_ISSET(serverSocket, &read)) {
				sockaddr_in clientAddr;
				int size = sizeof(clientAddr);

				SOCKET clientSocket = accept(serverSocket, (sockaddr*) &clientAddr, &size);
				if (clientSocket == INVALID_SOCKET) {
					// ???
					std::tcerr << _T("Cannot accept incoming connection: error code ") << WSAGetLastError() << std::endl;
					return;
				}

				if (connections.size() + 1 >= FD_SETSIZE) {
					std::tcerr << _T("Dropping connection from ") << inet_ntoa(clientAddr.sin_addr) << ":" << ntohs(clientAddr.sin_port) << std::endl;
					disconnect(clientSocket);
				} else {
					std::tcout << _T("Received connection from ") << inet_ntoa(clientAddr.sin_addr) << ":" << ntohs(clientAddr.sin_port) << std::endl;

					RedirectedConnection *con = new RedirectedConnection();
					con->clientSocket = clientSocket;
					con->clientBufLen = 0;
					con->targetBufLen = 0;
					con->targetReady = false;
					#ifdef _DEBUG_SOCKET_LIFECYCLE
						std::tcout << _T("New client socket ") << clientSocket << std::endl;
					#endif

					SOCKET targetSocket = startConnection(con, targetAddr);
					if (targetSocket == INVALID_SOCKET) {
						std::tcerr << _T("Failed connecting to ") << inet_ntoa(((sockaddr_in*) targetAddr)->sin_addr) << ":" << ntohs(((sockaddr_in*) targetAddr)->sin_port) << std::endl;
						disconnect(clientSocket);
						delete con;
					} else {
						#ifdef _DEBUG_SOCKET_LIFECYCLE
							std::tcout << _T("New target socket ") << targetSocket << std::endl;
						#endif
						con->targetSocket = targetSocket;

						unsigned long nonBlockingIO = true;
						ioctlsocket(clientSocket, FIONBIO, &nonBlockingIO);

						connections.push_back(con);
					}
				}
			} else if (FD_ISSET(serverSocket, &exc)) {
				int err;
				int size = sizeof(err);
				getsockopt(serverSocket, SOL_SOCKET, SO_ERROR, (char*) &err, &size);
				std::tcerr << _T("Listener failure: error code ") << err << std::endl;
			}

			for (std::list<RedirectedConnection*>::iterator it = connections.begin(); it != connections.end();) {
				RedirectedConnection *con = (*it);
				bool dc = false;

				if (FD_ISSET(con->clientSocket, &exc)) {
					dc = true;
					FD_CLR(con->clientSocket, &exc);
				} else if (FD_ISSET(con->targetSocket, &exc)) {
					dc = true;
					FD_CLR(con->targetSocket, &exc);
				} else {
					if (FD_ISSET(con->clientSocket, &read)) {
						//std::tcout << _T("Reading data from client") << std::endl;
						dc |= !readFromClient(con);
						FD_CLR(con->clientSocket, &read);
					}
					if (FD_ISSET(con->targetSocket, &write)) {
						//std::tcout << _T("Sending data to target") << std::endl;
						if (!con->targetReady) {
							con->targetReady = true;
							std::tcout << "Successfully connected to target." << std::endl;
						} else {
						dc |= !sendToTarget(con);
						FD_CLR(con->targetSocket, &write);
						}
					}
					if (FD_ISSET(con->targetSocket, &read)) {
						//std::tcout << _T("Reading data from target") << std::endl;
						dc |= !readFromTarget(con);
						FD_CLR(con->targetSocket, &read);
					}
					if (FD_ISSET(con->clientSocket, &write)) {
						//std::tcout << _T("Sending data to client") << std::endl;
						dc |= !sendToClient(con);
						FD_CLR(con->clientSocket, &write);
					}
				}

				if (dc) {
					std::tcerr << _T("Connection closed.") << std::endl;
					disconnect(con->clientSocket);
					disconnect(con->targetSocket);
					connections.erase(it++);
					delete con;
				} else
					++it;
			}
		}
	}
}

int setupSocketSets(fd_set *read, fd_set *write, fd_set *exc, SOCKET &serverSocket) {
	FD_ZERO(read);
	FD_ZERO(write);
	FD_ZERO(exc);

	FD_SET(serverSocket, read);
	FD_SET(serverSocket, exc);

	SOCKET maxFd = serverSocket;
	for (std::list<RedirectedConnection*>::iterator it = connections.begin(); it != connections.end(); ++it) {
		RedirectedConnection* con = (*it);
		if (!con->targetReady || con->clientBufLen > 0)
			FD_SET(con->targetSocket, write);
		if (con->clientBufLen < NET_IO_BUFFER_SIZE)
			FD_SET(con->clientSocket, read);
		if (con->targetBufLen > 0)
			FD_SET(con->clientSocket, write);
		if (con->targetReady && con->targetBufLen < NET_IO_BUFFER_SIZE)
			FD_SET(con->targetSocket, read);
		FD_SET(con->clientSocket, exc);
		FD_SET(con->targetSocket, exc);

		if (con->clientSocket > maxFd)
			maxFd = con->clientSocket;
		if (con->targetSocket > maxFd)
			maxFd = con->clientSocket;
	}
	return (int)maxFd;
}

bool disconnect(SOCKET clientSocket) {
	#ifdef _DEBUG_SOCKET_LIFECYCLE
		std::tcout << "Disconnected socket " << clientSocket << std::endl;
	#endif
	if (shutdown(clientSocket, FD_WRITE) != 0)
		return false;
	
	for (char buf[NET_IO_BUFFER_SIZE];;) {
		int count = recv(clientSocket, buf, NET_IO_BUFFER_SIZE, 0);
		if (count == SOCKET_ERROR)
			return false;

		if (count == 0)
			break;
	}

	if (closesocket(clientSocket) != 0)
		return false;

	return true;
}

bool readFromClient(RedirectedConnection *con) {
	int count = recv(con->clientSocket, con->clientBuf + con->clientBufLen, NET_IO_BUFFER_SIZE - con->clientBufLen, 0);
	if (count == 0) {
		#ifdef _DEBUG_DATA_TRANSFER
			std::tcerr << _T("Client closed connection.") << con->clientSocket << std::endl;
		#endif
		return false;
	}

	if (count == SOCKET_ERROR) {
		int err;
		int size = sizeof(err);
		getsockopt(con->clientSocket, SOL_SOCKET, SO_ERROR, (char*) &err, &size);
		return (err == WSAEWOULDBLOCK);
	}

	con->clientBufLen += count;

	#ifdef _DEBUG_DATA_TRANSFER
		std::tcout << _T("Read ") << count << _T(" bytes from client") << std::endl;
	#endif
	return true;
}

bool sendToClient(RedirectedConnection *con) {
	int count = send(con->clientSocket, con->targetBuf, con->targetBufLen, 0);
	if (count == SOCKET_ERROR) {
		int err;
		int size = sizeof(err);
		getsockopt(con->clientSocket, SOL_SOCKET, SO_ERROR, (char*) &err, &size);
		return (err == WSAEWOULDBLOCK);
	}

	if (count < con->targetBufLen) {
		con->targetBufLen -= count;
		memmove(con->targetBuf, con->targetBuf + count, con->targetBufLen);
	} else
		con->targetBufLen = 0;

	#ifdef _DEBUG_DATA_TRANSFER
		std::tcout << _T("Sent ") << count << _T(" bytes to client") << std::endl;
	#endif

	return true;
}

unsigned long resolveAddress(const _TCHAR* hostname) {
	unsigned long ip = _tinet_addr(hostname);
	if (ip == INADDR_NONE) {
		char* name = _tNewValueOf(hostname);
		hostent* host = gethostbyname(name);
		delete name;
		if (host == NULL)
			return INADDR_NONE;

		ip = *((unsigned long*) host->h_addr_list[0]);
	}

	return ip;
}

SOCKET startConnection(RedirectedConnection *con, sockaddr *targetAddr) {
	SOCKET targetSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (targetSocket == INVALID_SOCKET)
		return targetSocket;
	
	unsigned long nonBlocking = true;
	ioctlsocket(targetSocket, FIONBIO, &nonBlocking);

	int result = connect(targetSocket, targetAddr, sizeof((*targetAddr)));
	con->targetReady = (result == 0);
	if (result == SOCKET_ERROR) {
		int err = WSAGetLastError();
		if (err == WSAEWOULDBLOCK)
			con->targetReady = false;
		else
			return INVALID_SOCKET;
	}
	
	return targetSocket;
}

bool readFromTarget(RedirectedConnection *con) {
	int count = recv(con->targetSocket, con->targetBuf + con->targetBufLen, NET_IO_BUFFER_SIZE - con->targetBufLen, 0);
	if (count == 0) {
		#ifdef _DEBUG_DATA_TRANSFER
			std::tcerr << _T("Target closed connection.") << std::endl;
		#endif
		return false;
	}

	if (count == SOCKET_ERROR) {
		int err;
		int size = sizeof(err);
		getsockopt(con->targetSocket, SOL_SOCKET, SO_ERROR, (char*) &err, &size);
		return (err == WSAEWOULDBLOCK);
	}

	con->targetBufLen += count;
	#ifdef _DEBUG_DATA_TRANSFER
		std::tcout << _T("Read ") << count << _T(" bytes from target") << std::endl;
	#endif
	return true;
}

bool sendToTarget(RedirectedConnection *con) {
	int count = send(con->targetSocket, con->clientBuf, con->clientBufLen, 0);
	if (count == SOCKET_ERROR) {
		int err;
		int size = sizeof(err);
		getsockopt(con->targetSocket, SOL_SOCKET, SO_ERROR, (char*) &err, &size);
		return (err == WSAEWOULDBLOCK);
	}

	if (count < con->clientBufLen) {
		con->clientBufLen -= count;
		memmove(con->clientBuf, con->clientBuf + count, con->clientBufLen);
	} else
		con->clientBufLen = 0;

	#ifdef _DEBUG_DATA_TRANSFER
		std::tcout << _T("Sent ") << count << _T(" bytes to target") << std::endl;
	#endif

	return true;
}
